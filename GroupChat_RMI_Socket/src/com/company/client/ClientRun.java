package com.company.client;

import com.company.server.SocketServerThread;

import java.rmi.RemoteException;
public class ClientRun {

    public static void main(String[] args) throws RemoteException, InterruptedException {
        Thread rmi = new Thread(new RMIClientThread(1099, "rmi://192.168.1.241/ABC"));
        rmi.start();

        Thread socket = new Thread(new SocketClientThread("192.168.1.241", 1100));
        socket.start();
    }

}
