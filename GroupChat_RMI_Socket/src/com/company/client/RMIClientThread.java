package com.company.client;

import com.company.model.Message;
import com.company.model.User;
import com.company.server.IServer;
import com.company.server.ServerImp;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Scanner;

public class RMIClientThread extends Thread {

    private int port;
    private String name;

    public RMIClientThread(int port, String name) {
        this.port = port;
        this.name = name;
    }

    @Override
    public void run() {
        try {
            // Enter username
            Scanner in = new Scanner(System.in);
            System.out.print("Your name? >> ");
            String userName = in.nextLine().trim();
            User user = new User(userName);

            // Get chatServer (stub) from RMI registry
            IServer chatServer = (IServer)Naming.lookup(name);
            System.out.println("Connect OK");
            //// ERROROR
            ClientImp chatClient = new ClientImp(user, chatServer);

            while (true) {
                String content = in.nextLine().trim();
                Message msg = new Message(user.getId(), user.getName(), content);
                chatServer.addMessageToQueue(msg);
                chatServer.notifyToBroadcast(1);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

}
