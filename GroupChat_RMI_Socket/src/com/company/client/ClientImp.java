package com.company.client;
import com.company.model.Message;
import com.company.model.User;
import com.company.server.IServer;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.UUID;

public class ClientImp extends UnicastRemoteObject implements IClient {

    private User user;
    private IServer chatServer;

    public ClientImp(User user, IServer chatServer) throws RemoteException {
        this.user = user;
        this.chatServer = chatServer;

        // Broadcast status message to server
        String content = "joined the room.";
        Message msg = new Message(this.user.getId(), this.user.getName(), content);

        // Regis
        /// ERRROROOROR
        chatServer.registerClient(this);

        // Broadcast
        chatServer.notifyClientStatus(msg);

    }

    public User getUser(){
        return user;
    }

    @Override
    public UUID getUserIdRemote() throws RemoteException {
        return user.getId();
    }

    @Override
    public void retrieveMessage(Message msg) throws RemoteException {
        System.out.println("[" + msg.getcName() + "]: " + msg.getContent());
    }

}
