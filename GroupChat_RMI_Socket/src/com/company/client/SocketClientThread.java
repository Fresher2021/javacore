package com.company.client;

import com.company.model.Message;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;

public class SocketClientThread extends Thread {


    private String hostname;
    private int port;

    public SocketClientThread(String hostname, int port){
        this.hostname = hostname;
        this.port = port;
    }

    public void run(){

        try (Socket socket = new Socket(hostname, port)) {

            InputStream input = socket.getInputStream();
            ObjectInputStream is = new ObjectInputStream(input);

            while(true){
                Message msg = (Message) is.readObject();
                System.out.println("[" + msg.getcName() + "]: " + msg.getContent());
            }

        } catch (UnknownHostException ex) {

            System.out.println("Server not found: " + ex.getMessage());

        } catch (IOException ex) {

            System.out.println("I/O error: " + ex.getMessage());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
