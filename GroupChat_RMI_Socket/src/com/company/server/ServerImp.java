package com.company.server;

import com.company.client.IClient;
import com.company.model.Message;
import com.company.model.User;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;

public class ServerImp extends UnicastRemoteObject implements IServer {

    private int flag;

    private Stack<IClient> clients;
    private Queue<Message> unsentMessages;
    private List<Message> messages;

    public ServerImp() throws RemoteException{
        this.clients  = new Stack<>();
        this.unsentMessages = new LinkedList<>();
        this.messages  = new ArrayList<>();
    }

    public synchronized int getFlag() {
        return flag;
    }

    // Broadcast message
    public synchronized void setFlag(int flag) {
        this.flag = flag;
    }

    public synchronized Queue<Message> getUnsentMessages() {
        return unsentMessages;
    }

    public synchronized Message getFirstMessage(){
        return unsentMessages.peek();
    }

    public synchronized void removeFirstMessage(){
        if(!getUnsentMessages().isEmpty()) unsentMessages.remove();
    }

    // Check Sender
    public synchronized IClient getConnectedClient(){
        return clients.peek();
    }

    @Override
    public void notifyClientStatus(Message msg) throws RemoteException {
        // Send message to other client
        for(IClient client : clients)
            // Print the message in client console
            if(!client.getUserIdRemote().equals(msg.getcId())) client.retrieveMessage(msg);

        // Print the message in server console
        System.out.println("[" + msg.getcName() + "]: " +  msg.getContent());
        System.out.println("[" + msg.getTime() + "]");
        System.out.println("----------------------");

    }

    @Override
    public synchronized void addMessageToQueue(Message msg) throws RemoteException {
        unsentMessages.offer(msg);
//        System.out.println("Message added to QUEUE!!!! " + unsentMessages.size());
    }

    @Override
    public synchronized void notifyToBroadcast(int flag) throws RemoteException {
        this.flag = flag;
    }

    @Override
    public synchronized void saveMessage(Message msg){
        if(!messages.contains(msg)){
            messages.add(msg);
            System.out.println("[" + msg.getcName() + "]: " + msg.getContent());
        }
    }

    @Override
    public synchronized void registerClient(IClient client) throws RemoteException {
        this.clients.push(client);
    }

}
