package com.company.server;

import com.company.model.Message;

import java.io.*;
import java.net.*;
import java.util.UUID;

public class SocketServerThread extends Thread {

    private Socket socket;
    private ServerImp server;

    public SocketServerThread(Socket socket, ServerImp server) {
        this.socket = socket;
        this.server = server;
    }

    public void run() {
        try {
            OutputStream output = socket.getOutputStream();
            ObjectOutputStream os = new ObjectOutputStream(output);

            while(true){
                if(server.getUnsentMessages().size()>0 && server.getFlag()==1){
                        Message msg = server.getFirstMessage();
                        os.writeObject(msg);
//                        System.out.println("MESSAGE SENT!!!!");

                        Thread.sleep(200);
                        if(server.getUnsentMessages().size()>0 && server.getFlag()==1){
                            // Save message
                            server.saveMessage(msg);

                            // Remove message from queue
                            server.removeFirstMessage();
                        }
                        server.setFlag(0);
                }
            }

        } catch (IOException | InterruptedException ex) {
            System.out.println("Server exception: " + ex.getMessage());
            ex.printStackTrace();
        }
    }
}
