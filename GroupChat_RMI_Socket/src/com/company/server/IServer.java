package com.company.server;

import com.company.client.IClient;
import com.company.model.Message;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IServer extends Remote {
    public void notifyClientStatus(Message msg) throws RemoteException;
    public void addMessageToQueue(Message msg) throws RemoteException;
    public void notifyToBroadcast(int flag) throws RemoteException;
    public void saveMessage(Message msg) throws RemoteException;
    public void registerClient(IClient client) throws RemoteException;
}
