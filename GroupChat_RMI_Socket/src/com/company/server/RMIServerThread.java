package com.company.server;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

public class RMIServerThread extends Thread {

    private String url;
    private int port;
    private ServerImp server;


    public RMIServerThread(String url, int port, ServerImp server){
        this.url = url;
        this.port = port;
        this.server = server;
    }

    public void run(){
        try{
            // register a name to RMI registry, for binding
            LocateRegistry.createRegistry(this.port);
            Naming.rebind(url, this.server);
            System.out.println("RMI server running on port " + port + "...");
            System.out.println("------------------");
        } catch (RemoteException | MalformedURLException e){
            System.out.println("[System] Server failed: " + e);
        }
    }

}
