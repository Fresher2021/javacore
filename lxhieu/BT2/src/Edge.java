
public class Edge {
	private int length;
	private int cost;
	private Node start;
	private Node target;

	public Edge(int length, int cost, Node start, Node target) {
		this.length = length;
		this.start = start;
		this.target = target;
		this.cost = cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getCost() {
		return cost;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public Node getStartNode() {
		return start;
	}

	public void setStarNode(Node start) {
		this.start = start;
	}

	public Node getTargetNode() {
		return target;
	}

	public void setTargetNode(Node target) {
		this.target = target;
	}

}