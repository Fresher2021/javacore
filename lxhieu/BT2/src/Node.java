import java.util.ArrayList;
import java.util.List;

public class Node implements Comparable<Node> {

	private boolean visited;
	private String name;
	private List<Edge> listEdge;
	private int distance = Integer.MAX_VALUE;
	private int money = Integer.MAX_VALUE;
	private int money2 = Integer.MIN_VALUE;
	private Node previous;

	public Node(String name) {
		this.name = name;
		this.listEdge = new ArrayList<>();
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}

	public int getDistance() {
		return distance;
	}

	public List<Edge> getListEdge() {
		return listEdge;
	}

	public String getName() {
		return name;
	}

	public Node getPrevious() {
		return previous;
	}

	public boolean isVisited() {
		return visited;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public void setListEdge(List<Edge> listEdge) {
		this.listEdge = listEdge;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPrevious(Node previous) {
		this.previous = previous;
	}

	public void setVisited(boolean visited) {
		this.visited = visited;
	}

	public void addNeighbour(Edge e) {
		this.listEdge.add(e);
	}

	public void setMoney2(int money2) {
		this.money2 = money2;
	}

	public int getMoney2() {
		return money2;
	}

	@Override
	public int compareTo(Node other) {
		return Integer.compare(this.distance, other.getDistance());
	}

//	@Override
//	public int compareTo(Node other) {
//		return Integer.compare(this.distance, other.getDistance());
//	}
}
