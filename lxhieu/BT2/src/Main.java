import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

public class Main {

	public static void dijkstra1(Node o) {
		o.setDistance(0);
		o.setMoney(0);
		PriorityQueue<Node> priorityQueue = new PriorityQueue<>();
		priorityQueue.add(o);
		o.setVisited(true);
		while (!priorityQueue.isEmpty()) {
			Node currentNode = priorityQueue.poll();
			for (Edge edge : currentNode.getListEdge()) {
				Node v = edge.getTargetNode();
				if (!v.isVisited()) {
					int newDistance = currentNode.getDistance() + edge.getLength();
					int newCost = currentNode.getMoney() + edge.getCost();
					if (newCost < v.getMoney()) {
						priorityQueue.remove(v);
						v.setDistance(newDistance);
						v.setPrevious(currentNode);
						v.setMoney(newCost);
						priorityQueue.add(v);
					}
				}
			}
			currentNode.setVisited(true);
		}
	}

	public static void dijkstra2(Node o) {
		o.setDistance(0);
		o.setMoney(0);
		PriorityQueue<Node> priorityQueue = new PriorityQueue<>();
		priorityQueue.add(o);
		o.setVisited(true);
		while (!priorityQueue.isEmpty()) {
			Node currentNode = priorityQueue.poll();
			for (Edge edge : currentNode.getListEdge()) {
				Node v = edge.getTargetNode();
				if (!v.isVisited()) {
					int newDistance = currentNode.getDistance() + edge.getLength();
					int newCost = currentNode.getMoney() + edge.getCost();
					if (newDistance < v.getDistance()) {
						priorityQueue.remove(v);
						v.setDistance(newDistance);
						v.setPrevious(currentNode);
						v.setMoney(newCost);
						priorityQueue.add(v);
					}
				}
			}
			currentNode.setVisited(true);
		}
	}

	public static void dijkstra3(Node o) {
		o.setDistance(0);
		o.setMoney2(0);
		PriorityQueue<Node> priorityQueue = new PriorityQueue<>();
		priorityQueue.add(o);
		o.setVisited(true);
		while (!priorityQueue.isEmpty()) {
			Node currentNode = priorityQueue.poll();
			for (Edge edge : currentNode.getListEdge()) {
				Node v = edge.getTargetNode();
				if (!v.isVisited()) {
					int newDistance = currentNode.getDistance() + edge.getLength();
					int newCost = currentNode.getMoney2() + edge.getCost();
					if (newCost > v.getMoney2()) {
						priorityQueue.remove(v);
						v.setDistance(newDistance);
						v.setPrevious(currentNode);
						v.setMoney2(newCost);
						priorityQueue.add(v);
					}
				}
			}
//			currentNode.setVisited(true);
		}
	}

	public static void getPath(Node target) {
		List<Node> path = new ArrayList<>();
		for (Node e = target; e != null; e = e.getPrevious()) {
			path.add(e);
		}
		Collections.reverse(path);
		System.out.print("Path: ");
		for (Node i : path) {
			System.out.print(i.getName() + " ");
		}
		System.out.println();
	}

	public static void main(String[] args) {
		Node a = new Node("A");
		Node b = new Node("B");
		Node c = new Node("C");
		Node d = new Node("D");

		a.addNeighbour(new Edge(5, 3, a, b));
		a.addNeighbour(new Edge(2, 5, a, c));
		b.addNeighbour(new Edge(1, 4, b, c));
		b.addNeighbour(new Edge(4, 1, b, d));
		c.addNeighbour(new Edge(3, 2, c, d));

		// Find length with mininum cost
		dijkstra1(a);
		getPath(d);
		System.out.println("Min cost from A to D: " + d.getMoney() + " with length: " + d.getDistance());

//		// Find cost with mininum length
//		dijkstra2(a);
//		getPath(d);
//		System.out.println("Min length from A to D: " + d.getDistance() + " with cost: " + d.getMoney());

		// Find length with maximum cost
//		dijkstra3(a);
//		getPath(d);
//		System.out.println("Max cost from A to F: " + d.getMoney2() + " with length: " + d.getDistance());

	}
}
