package Server;

import java.io.*;
import java.net.Socket;
import java.util.List;

public class ServiceThread extends Thread {
    private String clientName;
    private Socket socketOfServer;
    private List<Message> messageList;

    public ServiceThread(String clientName, Socket socketOfServer, List<Message> messageList) {
        this.clientName = clientName;
        this.socketOfServer = socketOfServer;
        this.messageList = messageList;
        System.out.println("New connection with client # " + this.clientName + " at " + socketOfServer);
    }

    @Override
    public void run() {
        try {
            BufferedReader is = new BufferedReader(new InputStreamReader(socketOfServer.getInputStream()));
            BufferedWriter os = new BufferedWriter(new OutputStreamWriter(socketOfServer.getOutputStream()));

            int prevSize = 0;
            while (true) {
                Thread.sleep(100);
                if (messageList.size() > prevSize) {
                    if (!clientName.equals(messageList.get(messageList.size() - 1).getName())) {
                        os.write(messageList.get(messageList.size() - 1).getMessString());
                        os.newLine();
                        os.flush();
                    }
                    prevSize = messageList.size();
                }
            }

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}