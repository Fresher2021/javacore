import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BodyComponent } from './body/body.component';
import { BuySellComponent } from './buy-sell/buy-sell.component';
import { CarouselComponent } from './carousel/carousel.component';
import { ExportComponent } from './export/export.component';
import { RateComponent } from './rate/rate.component';

const routes: Routes = [
  { path:'', component: BodyComponent},
  { path:'rate', component: RateComponent},
  { path:'buy-sell',component: BuySellComponent},
  { path:'export',component:ExportComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
