package com.example.bt1;

import java.util.ArrayList;
import java.util.Collections;

public class Subject {
	String name;
	ArrayList<Student> listStudents;

	public Subject(String name, ArrayList<Student> listStudents) {
		this.name = name;
		this.listStudents = listStudents;
	}

	public void sortByScore() {
		for (int i = 0; i < listStudents.size() - 1; i++) {
			for (int j = 0; j < listStudents.size() - i - 1; j++) {
				if (listStudents.get(j).score < listStudents.get(j + 1).score) {
					Collections.swap(listStudents, j, j + 1);
				}
			}
		}
	}

	public void displayTopScore(int n) {
		int i = 0;
		while (i < n) {
			int count = 1;
			System.out.println(name + " - " + listStudents.get(i).name + " - " + listStudents.get(i).score);
			for (int j = i + 1; j < listStudents.size(); j++) {
				if (listStudents.get(j).score == listStudents.get(i).score) {
					System.out.println(name + " - " + listStudents.get(j).name + " - " + listStudents.get(j).score);
					count++;
				} else {
					break;
				}
			}
			if (count >= n) {
				break;
			} else {
				i += count;
			}
		}
	}

}
