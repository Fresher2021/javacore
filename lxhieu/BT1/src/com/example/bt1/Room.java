package com.example.bt1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Room {
	String name;
	ArrayList<Subject> listSubjects;
	ArrayList<Subject> listSubjects2;

	ArrayList<Student> listStudent;

	public Room(String name) {
		this.name = name;
		listStudent = new ArrayList<>();
	}

	public void totalScore() {
		for (int i = 0; i < listSubjects2.get(0).listStudents.size(); i++) {
			int total = 0;
			for (int j = 0; j < listSubjects2.size(); j++) {
				for (int k = 0; k < listSubjects2.get(j).listStudents.size(); k++) {
					if (listSubjects2.get(j).listStudents.get(k).id == i) {
						total += listSubjects2.get(j).listStudents.get(k).score;
						break;
					}
				}
			}
			listSubjects2.get(0).listStudents.get(i).score = total;
			listStudent.add(listSubjects2.get(0).listStudents.get(i));
		}
	}

	public void sortByTotalScore() {
		for (int i = 0; i < listStudent.size() - 1; i++) {
			for (int j = 0; j < listStudent.size() - i - 1; j++) {
				if (listStudent.get(j).score < listStudent.get(j + 1).score) {
					Collections.swap(listStudent, j, j + 1);
				}
			}
		}
	}

	public void displayTopTotalScore(int n) {
		for (int i = 0; i < n; i++) {
			System.out.println(name + " - " + listStudent.get(i).name + " - " + listStudent.get(i).score);
		}
	}

	
	public void displayTopScore(int n) {
		for (int i = 0; i < listSubjects.size(); i++) {
			listSubjects.get(i).sortByScore();
			listSubjects.get(i).displayTopScore(n);
			System.out.println("-------------");

		}
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();

		ArrayList<Student> listOfMath = new ArrayList<>();
		listOfMath.add(new Student(0, "Hieu", 10));
		listOfMath.add(new Student(1, "Le", 10));
		listOfMath.add(new Student(2, "Xuan", 9));
		listOfMath.add(new Student(3, "Ha", 9));
		listOfMath.add(new Student(4, "Ku", 8));

		Subject math = new Subject("Math", listOfMath);

		ArrayList<Student> listOfArt = new ArrayList<>();
		listOfArt.add(new Student(0, "Hieu", 10));
		listOfArt.add(new Student(1, "Le", 9));
		listOfArt.add(new Student(2, "Xuan", 10));
		listOfArt.add(new Student(3, "Ha", 8));
		listOfArt.add(new Student(4, "Ku", 9));

		Subject art = new Subject("Art", listOfArt);

		ArrayList<Subject> listSubjects = new ArrayList<>();
		listSubjects.add(math);
		listSubjects.add(art);
		Room roomA = new Room("Room A");
		roomA.listSubjects = listSubjects;

		ArrayList<Subject> listSubjects2 = new ArrayList<>();
		for (Subject i : listSubjects) {
			listSubjects2.add(i);
		}

		roomA.listSubjects2 = listSubjects2;

		roomA.displayTopScore(n);

		roomA.totalScore();
		roomA.sortByTotalScore();
		roomA.displayTopTotalScore(n);

	}

}
