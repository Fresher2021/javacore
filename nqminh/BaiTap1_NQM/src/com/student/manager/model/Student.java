package com.student.manager.model;

import java.io.Serializable;


public class Student implements Serializable {
    private int number;
    private String className;
    private String studentName;
    private float mathScore;
    private float englishScore;
    private float totalScore = englishScore + mathScore;


    public Student() {
    }



    public Student(int number, String className, String studentName, float mathScore, float englishScore, float totalScore) {
        super();
        this.number = number;
        this.className = className;
        this.mathScore = mathScore;
        this.studentName = studentName;
        this.englishScore = englishScore;
        this.totalScore = totalScore;

    }

    //getter & setter
    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public float getMathScore() {
        return mathScore;
    }

    public void setMathScore(float mathScore) {
        this.mathScore = mathScore;
    }

    public float getEnglishScore() {
        return englishScore;
    }

    public void setEnglishScore(float englishScore) {
        this.englishScore = englishScore;
    }

    public float getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(float totalScore) {
        this.totalScore = totalScore;
    }
}