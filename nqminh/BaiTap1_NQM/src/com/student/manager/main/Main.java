package com.student.manager.main;

import java.io.IOException;

import com.student.manager.controller.StudentManager;


//main program
public class Main {


    public static void main(String[] args) throws IOException {

        StudentManager manager = new StudentManager();
        manager.showMenu();

    }

}
