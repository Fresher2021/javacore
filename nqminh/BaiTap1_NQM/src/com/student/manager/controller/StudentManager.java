package com.student.manager.controller;

import java.io.EOFException;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;


import com.student.manager.model.Student;

public class StudentManager {


    public static Scanner scanner = new Scanner(System.in);//User input
    public static List<Student> studentList;
    private static StudentDAO totxt;
    boolean exit = false;

    //add a student to list
    public void add() {
        int number = (studentList.size() > 0) ? (studentList.size() + 1) : 1;
        System.out.println("Number in list = " + number);
        String className = inputClassName();
        String studentName = inputStudentName();
        float mathScore = inputMathScore();
        float englishScore = inputEnglishScore();
        float totalScore = englishScore + mathScore;
        Student student = new Student(number, className, studentName, mathScore, englishScore, totalScore);
        studentList.add(student);
        totxt.write(studentList);
    }


    // sort top 3 math score
    public void sortMathScore() {
        Collections.sort(studentList, new SortMathScore());
        for (int i = 0; i < 3; i++) {
            showMath(studentList.get(i));
        }

    }

    public void sortEnglishScore() {
        Collections.sort(studentList, new SortEnglishScore());
        for (int j = 0; j < 3; j++) {
            showEnglish(studentList.get(j));
        }

    }

    //sort top 3 total score
    public void sortTotalScore() {
        Collections.sort(studentList, new SortTotalScore());
        for (int k = 0; k < 3; k++) {
            showTotal(studentList.get(k));
        }
    }


    /**
     * public void showAllStudent() {
     * <p>
     * System.out.print(studentList.size());
     * for (Student student : studentList) {
     * System.out.println("---Number of student " + student.getNumber() + "---");
     * System.out.println("   Class: " + student.getClassName());
     * System.out.println("   Student: " + student.getStudentName());
     * System.out.println("   Math score: " + student.getMathScore());
     * System.out.println("   English score: " + student.getEnglishScore());
     * }
     * }
     */

    //output total score
    public void showTotal(Student student) {
        System.out.println("---Number of student " + student.getNumber() + "---");
        System.out.println("   Class: " + student.getClassName());
        System.out.println("   Student: " + student.getStudentName());
        System.out.println("   Total score: " + student.getTotalScore());
    }

    public void showMath(Student student) {
        System.out.println("---Number of student " + student.getNumber() + "---");
        System.out.println("   Class: " + student.getClassName());
        System.out.println("   Student: " + student.getStudentName());
        System.out.println("   Math score: " + student.getMathScore());
    }

    public void showEnglish(Student student) {
        System.out.println("---Number of student " + student.getNumber() + "---");
        System.out.println("   Class: " + student.getClassName());
        System.out.println("   Student: " + student.getStudentName());
        System.out.println("   English score: " + student.getEnglishScore());
    }


    public String inputClassName() {
        System.out.print("Input class name: ");
        return scanner.nextLine();
    }


    private String inputStudentName() {
        System.out.print("Input student name: ");
        return scanner.nextLine();
    }


    private float inputMathScore() {
        System.out.print("Input math score: ");
        return scanner.nextInt();
    }


    public float inputEnglishScore() {
        System.out.print("Input english score: ");
        return scanner.nextInt();
    }

    //menu to choose
    public void showMenu() throws EOFException {
        totxt = new StudentDAO();
        studentList = totxt.read();
        System.out.println("-----------menu------------");
        System.out.println("1. Add student list.");
        System.out.println("2. Sort top 3 in math score.");
        System.out.println("3. Sort top 3 in english score.");
        System.out.println("4. Sort top 3 in total score.");
        System.out.println("0. Exit.");
        System.out.println("---------------------------");
        System.out.println("Please choose an option: ");
        String choose;
        while (true) {

            choose = scanner.nextLine();
            switch (choose) {
                case "1":
                    add();
                    break;

                case "2":
                    sortMathScore();
                    break;

                case "3":
                    sortEnglishScore();
                    break;

                case "4":
                    sortTotalScore();
                    break;

                case "0":
                    System.out.println("exit!");
                    exit = true;
                    break;

            }
            if (exit) {
                break;
            }
            // show menu
            showMenu();
            scanner.close();
        }
    }
}
