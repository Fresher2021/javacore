package com.student.manager.controller;

import java.util.Comparator;

import com.student.manager.model.Student;

public class SortMathScore implements Comparator<Student> {
    @Override
    public int compare(Student student1, Student student2) {
        if (student1.getMathScore() < student2.getMathScore()) {
            return 1;
        }
        return -1;
    }


}
