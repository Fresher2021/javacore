package com.student.manager.controller;

import com.student.manager.model.Student;

import java.util.Comparator;

public class SortTotalScore implements Comparator<Student> {
    @Override
    public int compare(Student student1, Student student2) {
        if (student1.getTotalScore() < student2.getTotalScore()) {
            return 1;
        }
        return -1;
    }

}
