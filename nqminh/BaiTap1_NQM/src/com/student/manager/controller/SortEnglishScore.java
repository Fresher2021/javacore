package com.student.manager.controller;

import com.student.manager.model.Student;
import java.util.Comparator;
public class SortEnglishScore implements Comparator<Student> {
    @Override
    public int compare(Student student1, Student student2) {
        if (student1.getEnglishScore() < student2.getEnglishScore()) {
            return 1;
        }
        return -1;
    }
}
