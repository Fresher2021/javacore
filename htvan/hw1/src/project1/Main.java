package project1;
import java.util.*;  

public class Main {


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Course course1 = new Course("Math");
		course1.addStudent("van", "A", 1);
		course1.addStudent("w","A",4);
		course1.addStudent("e", "A", 4);
		course1.addStudent("q","A",6);
		course1.addStudent("t", "B", 10);
		course1.addStudent("j","B",9);
		
		Course course2 = new Course("English");
		course2.addStudent("van", "A", 9);
		course2.addStudent("w","A",10);
		course2.addStudent("e","A",10);
		course2.addStudent("q","A",9);
		course2.addStudent("t1", "B", 6);
		course2.addStudent("j1","B",9);
		
		
		ArrayList<Student> studentListA = new ArrayList<Student>();
		
		
		for (Student st: course1.studentList) {
			if (st.className.equals("A")) {
				studentListA.add(st);
			}	
		}
		
		ArrayList<Student> studentListA1 = new ArrayList<Student>();
		for (Student st: course2.studentList) {
			if (st.className.equals("A")) {
				studentListA1.add(st);
			}	
		}
		
//		System.out.println(studentListA.size());
		
		
		ArrayList<Student> gradeSumList = new ArrayList<Student>();
		for(int i = 0; i < studentListA.size(); i++) {
			for (int j = 0; j < studentListA1.size(); j++) { 
				if (studentListA.get(i).name.equals(studentListA1.get(j).name)) {
					gradeSumList.add(new Student(studentListA.get(i).name,"A",studentListA.get(i).grade+studentListA1.get(j).grade));
				}
			}
		}
		
		for(int i = 0; i < gradeSumList.size()-1; i++) {
			for (int j = 0; j < gradeSumList.size()-i-1; j++) {
				if (gradeSumList.get(j).grade < gradeSumList.get(j+1).grade) {
					Collections.swap(gradeSumList, j, j+1);
				}
			}
		}

		
		
		for(int i = 0; i < studentListA.size()-1; i++) {
			for (int j = 0; j < studentListA.size()-i-1; j++) {
				if (studentListA.get(j).grade < studentListA.get(j+1).grade) {
					Collections.swap(studentListA, j, j+1);
				}
			}
		}
		
		for(int i = 0; i < studentListA1.size()-1; i++) {
			for (int j = 0; j < studentListA1.size()-i-1; j++) {
				if (studentListA1.get(j).grade < studentListA1.get(j+1).grade) {
					Collections.swap(studentListA1, j, j+1);
				}
			}
		}
		
		
		//System.out.println(studentListA.size());
		
		
		Scanner myObj = new Scanner(System.in);  // Create a Scanner object
		System.out.print("Enter n = ");
	    int n = myObj.nextInt();
	    
	    int i = 0;
	    while (i<=n-1) {
	    	System.out.println("Lop A - mon Toan: " + studentListA.get(i).name);
	    	i += 1;	    	
	    	if (i == studentListA.size()) {
    			break;
    		}
	    	//System.out.println(studentListA.get(i));
	    	while (studentListA.get(i).grade == studentListA.get(i-1).grade) {
	    		System.out.println("Lop A - mon Toan: " + studentListA.get(i).name);
	    		i += 1;
	    		if (i == studentListA.size()) {
	    			break;
	    		}
	    	}
	    }
	    
	    i = 0;
	    while (i<=n-1) {
	    	System.out.println("Lop A - mon English: " + studentListA1.get(i).name);
	    	i += 1;	    	
	    	if (i == studentListA1.size()) {
    			break;
    		}
	    	//System.out.println(studentListA.get(i));
	    	while (studentListA1.get(i).grade == studentListA1.get(i-1).grade) {
	    		System.out.println("Lop A - mon English: " + studentListA1.get(i).name);
	    		i += 1;
	    		if (i == studentListA1.size()) {
	    			break;
	    		}
	    	}
	    }
	    
	    
		
		
	    
	    //Sum of grades
	    i = 0;
	    while (i<=n-1) {
	    	System.out.println("Lop A - Tong diem: " + gradeSumList.get(i).name);
	    	i += 1;	    	
	    	if (i == gradeSumList.size()) {
    			break;
    		}
	    	//System.out.println(studentListA.get(i));
	    	while (gradeSumList.get(i).grade == gradeSumList.get(i-1).grade) {
	    		System.out.println("Lop A - Tong diem: " + gradeSumList.get(i).name);
	    		i += 1;
	    		if (i == gradeSumList.size()) {
	    			break;
	    		}
	    	}
	    }
	 
	}

}
