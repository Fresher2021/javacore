package project1;

import java.util.ArrayList;

public class Course {
	String courseName;
	ArrayList<Student> studentList = new ArrayList<Student>();
	
	public Course(String name) {
		courseName = name;
	}
	
	public void addStudent(String studentName,String className, int grade) {
		studentList.add(new Student(studentName, className, grade));
	}

}
