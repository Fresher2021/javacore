import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BuysellComponent } from './components/buysell/buysell.component';
import { ExportComponent } from './components/export/export.component';
import { GoldrateComponent } from './components/goldrate/goldrate.component';
import { GoldtransactionComponent } from './components/goldtransaction/goldtransaction.component';
import { HomeComponent } from './components/home/home.component';

const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'goldrate', component: GoldrateComponent},
  {path: 'buysell', component: BuysellComponent},
  {path: 'export', component: ExportComponent},
  {path: '', component: HomeComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
