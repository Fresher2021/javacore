import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DollarrateComponent } from './dollarrate.component';

describe('DollarrateComponent', () => {
  let component: DollarrateComponent;
  let fixture: ComponentFixture<DollarrateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DollarrateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DollarrateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
