import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GoldtransactionComponent } from './goldtransaction.component';

describe('GoldtransactionComponent', () => {
  let component: GoldtransactionComponent;
  let fixture: ComponentFixture<GoldtransactionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GoldtransactionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GoldtransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
