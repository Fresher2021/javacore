import { TestBed } from '@angular/core/testing';

import { GoldDataServiceService } from './gold-data-service.service';

describe('GoldDataServiceService', () => {
  let service: GoldDataServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GoldDataServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
