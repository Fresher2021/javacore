import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { GoldtransactionComponent } from './components/goldtransaction/goldtransaction.component';
import { GoldrateComponent } from './components/goldrate/goldrate.component';
import { BuysellComponent } from './components/buysell/buysell.component';
import { ExportComponent } from './components/export/export.component';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { DollarrateComponent } from './components/dollarrate/dollarrate.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    GoldtransactionComponent,
    GoldrateComponent,
    BuysellComponent,
    ExportComponent,
    DollarrateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    Ng2GoogleChartsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
