package homework1;


public class Graph {	
	final static int INF = 10000;
	int numOfVertices;
	int[][] costMatrix;
	int[][] lengthMatrix;
	
	public Graph(int n) {
		this.numOfVertices = n;
		this.costMatrix = new int[n][n];
		this.lengthMatrix = new int[n][n];
	}
	
	
	public void add(char start, char end, int cost, int length) {
		int u = start - 'A';
		int v = end - 'A';
		this.costMatrix[u][v] = cost;
		//this.costMatrix[v][u] = cost;
		this.lengthMatrix[u][v] = length;
		//this.lengthMatrix[v][u] = length;
	}
	
	public void find_lowest_cost(char start,char end) {
		int[] trace = new int[numOfVertices];
		int s = start - 'A';
		int e = end - 'A';
		int[] minCost = new int[numOfVertices];
		for (int i = 0; i < numOfVertices; i++) {
			minCost[i] = INF;
		}
		minCost[s] = 0;
		
		for (int k = 0; k< numOfVertices; k++) {
			for (int u = 0; u< numOfVertices; u++) {
				for (int v = 0; v < numOfVertices; v++) {
					if (costMatrix[u][v] > 0) {
						if (minCost[u] + costMatrix[u][v] < minCost[v]) {
							minCost[v] = minCost[u] + costMatrix[u][v];
							trace[v] = u;
						}
					}
					
				}
			}
		}
		int result = 0;
		int t = e;
		while (t!= s) {
			result += lengthMatrix[trace[t]][t];
			t = trace[t];
		}
		System.out.println("Length of the path with lowest cost: " + result);
		System.out.println("Lowest cost: " + minCost[e]);
		
		
	}
	
	public void find_shortest_length(char start,char end) {
		int[] trace = new int[numOfVertices];
		int s = start - 'A';
		int e = end - 'A';
		int[] minLength = new int[numOfVertices];
		for (int i = 0; i < numOfVertices; i++) {
			minLength[i] = INF;
		}
		minLength[s] = 0;
		
		for (int k = 0; k< numOfVertices; k++) {
			for (int u = 0; u< numOfVertices; u++) {
				for (int v = 0; v < numOfVertices; v++) {
					if (lengthMatrix[u][v] > 0) {
						if (minLength[u] + lengthMatrix[u][v] < minLength[v]) {
							minLength[v] = minLength[u] + lengthMatrix[u][v];
							trace[v] = u;
						}
					}
					
				}
			}
			
		}
		
		int result = 0;
		int t = e;
		while (t!= s) {
			result += costMatrix[trace[t]][t];
			t = trace[t];
		}
		System.out.println("Cost of the path with shortest length: " + result);
		System.out.println("Minimum length: " + minLength[e]);
		
	}
	
	public void find_highest_cost(char start,char end) {
		int[] trace = new int[numOfVertices];
		int s = start - 'A';
		int e = end - 'A';
		int[] minCost = new int[numOfVertices];
		for (int i = 0; i < numOfVertices; i++) {
			minCost[i] = INF;
		}
		minCost[s] = 0;
		int[][] costMatrix1 = new int[numOfVertices][numOfVertices];
		for (int i = 0; i < numOfVertices; i++) {
			for (int j = 0; j < numOfVertices; j++) {
				costMatrix1[i][j] = -costMatrix[i][j];
			}
		}

		
		for (int k = 0; k< numOfVertices; k++) {
			for (int u = 0; u< numOfVertices; u++) {
				for (int v = 0; v < numOfVertices; v++) {
					if (costMatrix[u][v] > 0) {
						if (minCost[u] + costMatrix1[u][v] < minCost[v]) {
							minCost[v] = minCost[u] + costMatrix1[u][v];
							trace[v] = u;
						}
					}
					
				}
			}
		}
		int result = 0;
		int t = e;
		while (t!= s) {
			result += lengthMatrix[trace[t]][t];
			t = trace[t];
		}
		System.out.println("Length of the path with highest cost: " + result);
		int maxCost = -minCost[e]; 
		System.out.println("Highest cost: " + maxCost);
		
	}
	

}
