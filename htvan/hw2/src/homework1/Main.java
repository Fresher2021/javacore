package homework1;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Graph g = new Graph(4);  //generate a directed, weighted graph
		g.add('A', 'B', 1, 1);
		g.add('A', 'C', 3, 3);
		g.add('B', 'C' , 2, 2);
		g.add('A', 'D', 4, 4);
		//g.add('D', 'B', 6, 9);
		g.find_lowest_cost('A', 'D');
		g.find_shortest_length('A', 'D');
		g.find_highest_cost('A', 'D');
	}

}
