
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ArrayList<Student> listStudent = new ArrayList<>();
        ArrayList<Subject> listSubject = new ArrayList<>();
        ArrayList<Score> listScore = new ArrayList<>();

        fillData fd = new fillData();
        fd.fill(listStudent, listSubject, listScore);

        ArrayList<Score> listScore1 = new ArrayList<>();
        ArrayList<Score> listScore2 = new ArrayList<>();

        for(Score score: listScore) {
            if(score.getSubject().getSubjectName().equals("Math"))
                listScore1.add(score);
            if (score.getSubject().getSubjectName().equals("Literature"))
                listScore2.add(score);
        }

        // Sort score of each subject
        Collections.sort(listScore1, new Comparator<Score>() {
            @Override
            public int compare(Score o1, Score o2) {
                return (int) (o2.getScore()- o1.getScore());
            }
        });

        Collections.sort(listScore2, new Comparator<Score>() {
            @Override
            public int compare(Score o1, Score o2) {
                return (int) (o2.getScore()-o1.getScore());
            }
        });

        //Set level for array list
        int level = 1;
        for (int i =1; i<listScore1.size(); i++){
            if (listScore1.get(i).getScore() < listScore1.get(i-1).getScore()) {
                level++;
                listScore1.get(i).getStudent().setLevel(level);
            }
            else listScore1.get(i).getStudent().setLevel(level);
        }

        level = 1;
        for (int i =1; i<listScore2.size(); i++){
            if (listScore2.get(i).getScore() < listScore2.get(i-1).getScore()) {
                level++;
                listScore2.get(i).getStudent().setLevel(level);
            }
            else listScore2.get(i).getStudent().setLevel(level);
        }

        //find total score all student
        for(Student student: listStudent){
            float totalScore  = 0;
            for(Score score: listScore){
                if(student.getStudentName().equals(score.getStudent().getStudentName()))
                    totalScore += score.getScore();
            }
            student.setTotalScore(totalScore);
        }

        //Sort total score of each student
        Collections.sort(listStudent, new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                return (int) (o2.getTotalScore()-o1.getTotalScore());
            }
        });

        // Find total level
        int totalLevel = 1;
        for (int i=1; i <listStudent.size(); i++){
            if (listStudent.get(i).getTotalScore() < listStudent.get(i-1).getTotalScore()) {
                totalLevel++;
                listStudent.get(i).setTotalLevel(totalLevel);
            }
            else listStudent.get(i).setTotalLevel(totalLevel);
        }

        Scanner sc = new Scanner(System.in);
        int count = 1;
        int num = 0 ;
        num = sc.nextInt();

        // Math Student
        for(int i=0; i<listScore1.size();i++){
            if (listScore1.get(i).getStudent().getLevel()==1){
                System.out.println(listScore1.get(i));
                count++;
            } else if (count<=num){
                System.out.println(listScore1.get(i));
                count++;
            } else if (listScore1.get(i).getStudent().getLevel()==listScore1.get(i-1).getStudent().getLevel()){
                System.out.println(listScore1.get(i));
            } else break;
        }
        System.out.println();

        count=1;
        // Literature Student
        for(int i=0; i<listScore2.size();i++){
            if (listScore2.get(i).getStudent().getLevel()==1){
                System.out.println(listScore2.get(i));
                count++;
            } else if (count<=num){
                System.out.println(listScore2.get(i));
                count++;
            } else if (listScore2.get(i).getStudent().getLevel()==listScore2.get(i-1).getStudent().getLevel()){
                System.out.println(listScore2.get(i));
            } else break;
        }
        System.out.println();


        for (Student student: listStudent){
            if ((student.getTotalLevel()<=num))
                System.out.println(student);
        }
    }


}