import java.util.ArrayList;

public class fillData {
    public void fill(ArrayList<Student> listStudent, ArrayList<Subject> listSubject, ArrayList<Score> listScore)   {

        Student anh = new Student("Tuan Anh");
        Student ha = new Student("Quan Ha");
        Student hieu = new Student("Xuan Hieu");
        Student minh = new Student("Quang Minh");
        Student van = new Student("Thu Van");

        listStudent.add(anh);
        listStudent.add(ha);
        listStudent.add(hieu);
        listStudent.add(minh);
        listStudent.add(van);

        Subject math = new Subject("Math");
        Subject literature = new Subject("Literature");

        listSubject.add(math);
        listSubject.add(literature);

        Score anhScore1 = new Score(anh, math, 8);
        Score anhScore2 = new Score(anh, literature, 7);

        Score haScore1 = new Score(ha, math, 9);
        Score haScore2 = new Score(ha, literature, 7);

        Score hieuScore1 = new Score(hieu, math, 8);
        Score hieuScore2 = new Score(hieu, literature, 10);

        Score minhScore1 = new Score(minh, math, 9);
        Score minhScore2 = new Score(minh, literature, 7);

        Score vanScore1 = new Score(van, math, 10);
        Score vanScore2 = new Score(van, literature, 9);

        listScore.add(anhScore1);
        listScore.add(anhScore2);
        listScore.add(haScore1);
        listScore.add(haScore2);
        listScore.add(hieuScore1);
        listScore.add(hieuScore2);
        listScore.add(minhScore1);
        listScore.add(minhScore2);
        listScore.add(vanScore1);
        listScore.add(vanScore2);

        System.out.println("Input number: ");
    }
}
