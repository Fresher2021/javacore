public class Score {
    private Student student;
    private Subject subject;
    private float score;

    public Score() {}

    public Score(Student student, Subject subject, float score) {
        this.student = student;
        this.subject = subject;
        this.score = score;
    }

    public Student getStudent() { return student; }

    public Subject getSubject() { return subject; }

    public float getScore() { return score; }

    public String toString() {
        return student.getStudentName() + ", " + subject.getSubjectName() + ", " + score;
    }
}
