public class Student {

    private String studentName;

    private int level = 1;
    private int totalLevel=1;
    private float totalScore;

    public Student() {}

    public Student (String name){
        this.studentName = name;
    }

    public String getStudentName() {
        return studentName;
    }


    public int getLevel() {
        return  level;
    }

    public void setLevel(int lv) {
        level = lv;
    }

    public float getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(float totalScore) {
        this.totalScore = totalScore;
    }

    public int getTotalLevel() {
        return totalLevel;
    }

    public void setTotalLevel(int totalLevel) {
        this.totalLevel = totalLevel;
    }

    @Override
    public String toString(){
        return studentName + ", " + totalScore;
    }

}
