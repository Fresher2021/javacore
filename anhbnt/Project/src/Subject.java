import java.util.ArrayList;

public class Subject {

    private String subjectName;

    public Subject(){

    }

    public Subject(String subName) {
        this.subjectName = subName;
    }


    public String getSubjectName() {
        return subjectName;
    }


    @Override
    public String toString() {
        return subjectName;
    }
}

