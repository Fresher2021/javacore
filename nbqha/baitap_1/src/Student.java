
public class Student {
	private String name;
	private String des;
	private Course cl;
	
	// to compare
	private int level=1;
	
	// total score
	private float totalScore;
	private int totalLevel=1;
	
	public Student() {}
	
	public Student(String name, String des, Course cl) {
		super();
		this.name = name;
		this.des = des;
		this.cl = cl;
	}
	
	public String getName() {
		return name;
	}
	
	public int getLevel() {
		return level;
	}
	
	public void setLevel(int lv) {
		level = lv;
	}
	
	public float getTotalCore() {
		return totalScore;
	}
	
	public void setTotalScore(float totalScore) {
		this.totalScore = totalScore;
	}
	
	public int getTotalLevel() {
		return totalLevel;
	}

	public void setTotalLevel(int totalLevel) {
		this.totalLevel = totalLevel;
	}

	@Override
	public String toString() {
		if(des!=null)
			return name + ", " + des + ", " + cl + ", " + totalScore+"\n";
		else return name + ", " + cl + ", " + totalScore + " diem"+"\n";
	}

	public String outLevel() {
		return level+" ";
	}
}
