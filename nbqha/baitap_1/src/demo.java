import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.Scanner;

public class demo {
	public static void main(String[] args) {
		Data data = new Data();
		
		//data.fillRd(100);
		
		//ArrayList<Course> listC = data.getListC();
		//ArrayList<Student> listSt = data.getListSt();
		//ArrayList<Subject> listSb = data.getListSb();
		//ArrayList<Result> listRs = data.getListRs();
		
		ArrayList<Course> listC = new ArrayList<>();
		ArrayList<Student> listSt = new ArrayList<>();
		ArrayList<Subject> listSb = new ArrayList<>();
		ArrayList<Result> listRs = new ArrayList<>();
		
		data.fill(listC, listSt, listSb, listRs);
		//System.out.println(listSt);
		//System.out.println(listRs);
		
		
		ArrayList<Result> listRs_1 = new ArrayList<>();
		ArrayList<Result> listRs_2 = new ArrayList<>();
		
		for(Result rs: listRs) {
			if(rs.getSubject().getName().equals("Java")) listRs_1.add(rs);
			if(rs.getSubject().getName().equals("Angular")) listRs_2.add(rs);
		}
		
		Collections.sort(listRs_1, new Comparator<Result>() {
			@Override
			public int compare(Result rs1, Result rs2) {
				// TODO Auto-generated method stub
				
				return (int) (rs2.getScore()-rs1.getScore());
			}
		});
		
		Collections.sort(listRs_2, new Comparator<Result>() {
			@Override
			public int compare(Result rs1, Result rs2) {
				// TODO Auto-generated method stub
				
				return (int) (rs2.getScore()-rs1.getScore());
			}
		});
		
		// algo
		// class 1
		int level = 1;
		for(int i=1; i<listRs_1.size(); i++) {
			if(listRs_1.get(i).getScore() < listRs_1.get(i-1).getScore()) {
				level++;
				listRs_1.get(i).getStudent().setLevel(level);
			}
			else listRs_1.get(i).getStudent().setLevel(level);
			
		}
		System.out.println(listRs_1.get(0).getStudent().getLevel());
		// class 2
		level = 1;
		for(int i=1; i<listRs_2.size(); i++) {
			if(listRs_2.get(i).getScore() < listRs_2.get(i-1).getScore()) {
				level++;
				listRs_2.get(i).getStudent().setLevel(level);
			}
			else listRs_2.get(i).getStudent().setLevel(level);
			
		}
		
		
		// find total score all student
		for(Student st: listSt) {
			float totalScore = 0;
			for(Result rs: listRs) {
				if(st.getName().equalsIgnoreCase(rs.getStudent().getName())) totalScore += rs.getScore();
			}
			st.setTotalScore(totalScore);
		}
		
		// Sort by total score
		Collections.sort(listSt, new Comparator<Student>() {
			@Override
			public int compare(Student st1, Student st2) {
				// TODO Auto-generated method stub
				return (int) (st2.getTotalCore()-st1.getTotalCore());
			}
		});
		
		// Mark level
		int totalLevel = 1;
		for(int i=1; i<listSt.size(); i++) {
			if(listSt.get(i).getTotalCore() < listSt.get(i-1).getTotalCore()) {
				totalLevel++;
				listSt.get(i).setTotalLevel(totalLevel);
			}
			else listSt.get(i).setTotalLevel(totalLevel);
		}
		
		
		// OUT
		
		while(true) {
			Scanner sc = new Scanner(System.in);
			int in = sc.nextInt();
			
			if(in==-9) {
				System.out.println("BYE");
				break;
			}
			if(in<0) System.out.println("NHAP SO KHONG AM");
			else {
				// class 1
				System.out.println("Top "+in+" in Java class:");
				int c=1;
				for(int i=0; i<=listRs_1.size(); i++) {
					if(listRs_1.get(i).getStudent().getLevel()==1) {
							System.out.print("\t"+listRs_1.get(i));
							c++;
					} else {
						if(c<=in) {
							System.out.print("\t"+listRs_1.get(i));
							c++;
						} else {
							if(listRs_1.get(i).getStudent().getLevel()==listRs_1.get(i-1).getStudent().getLevel()) {
								System.out.print("\t"+listRs_1.get(i));
							} else break;
						}
					} 
				}
				
				System.out.println();
				// Class 2
				System.out.println("Top "+in+" in Angular class:");
				c=1;
				for(int i=0; i<=listRs_2.size(); i++) {
					if(listRs_2.get(i).getStudent().getLevel()==1) {
						
							System.out.print("\t"+listRs_2.get(i));
							c++;
						
					} else {
						if(c<=in) {
							System.out.print("\t"+listRs_2.get(i));
							c++;
						} else {
							if(listRs_2.get(i).getStudent().getLevel()==listRs_2.get(i-1).getStudent().getLevel()) {
								System.out.print("\t"+listRs_2.get(i));
							} else break;
						}
					} 
				}
				
				System.out.println();
				// total out
				System.out.println("Top "+in+" total score in both class:");
				c=1;
				for(int i=0; i<listSt.size(); i++) {
					if(listSt.get(i).getTotalLevel()==1) {
							System.out.print("\t"+listSt.get(i));
							c++;
					} else {
						if(c<=in) {
							System.out.print("\t"+listSt.get(i));
							c++;
						} else {
							if(listSt.get(i).getTotalLevel()==listSt.get(i-1).getTotalLevel()) {
								System.out.print("\t"+listSt.get(i));
							} else break;
						}
					}
				}
				// end
			}
		}
		
	}
}
