
public class Course {
	private String name;
	
	public Course() {}
	
	public Course(String name) {
		this.name = name;
	}
	
	public String getClassroom() {
		return name;
	}

	@Override
	public String toString() {
		return name;
	}
	
	
}
