
public class Subject {
	private String name;
	private int course;
	
	public Subject() {
		
	}
	
	public Subject(String name, int course) {
		this.name = name;
		this.course = course;
	}
	
	public String getName() {
		return name;
	}
	
	public int getCourse() {
		return course;
	}

	@Override
	public String toString() {
		return "Subject [name=" + name + ", course=" + course + "]";
	}
	
	
}
