
public class Result {
	private Student st;
	private Subject sb;
	private float score;
	
	public Result() {}
	
	public Result(Student st, Subject sb, float score) {
		this.st = st;
		this.sb = sb;
		this.score = score;
	}
	
	public Student getStudent() {
		return st;
	}
	
	public Subject getSubject() {
		return sb;
	}
	
	public float getScore() {
		return score;
	}
	
	public String toString() {
		return st.getName()+", "+st.getClass().getName()+", "+sb.getName()+", "+score+" diem." +"\n";
	}
	
	
}
