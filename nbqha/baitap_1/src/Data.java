import java.util.ArrayList;
import java.util.Random;

public class Data {
	private ArrayList<Course> listC = new ArrayList<>();
	private ArrayList<Student> listSt = new ArrayList<>();
	private ArrayList<Subject> listSb = new ArrayList<>();
	private ArrayList<Result> listRs = new ArrayList<>();

	public Data() {

	}

	public ArrayList<Course> getListC() {
		return listC;
	}

	public ArrayList<Student> getListSt() {
		return listSt;
	}

	public ArrayList<Subject> getListSb() {
		return listSb;
	}

	public ArrayList<Result> getListRs() {
		return listRs;
	}

	public void fill(ArrayList<Course> listCl, ArrayList<Student> listSt, ArrayList<Subject> listSb,
			ArrayList<Result> listRs) {
		Course cl = new Course("Fresher 27");
		Course cl2 = new Course("Fresher 28");

		// listCl = new ArrayList<>();
		listCl.add(cl);
		listCl.add(cl2);

		Student ha = new Student("Quang Ha", "", cl2);
		Student van = new Student("Thu Van", "", cl2);
		Student hieu = new Student("Xuan Hieu", "", cl2);
		Student anh = new Student("Tuan Anh", "", cl2);
		Student minh = new Student("Quang Minh", "", cl2);

		// listSt = new ArrayList<Student>();
		listSt.add(ha);
		listSt.add(van);
		listSt.add(hieu);
		listSt.add(anh);
		listSt.add(minh);

		Subject java = new Subject("Java", 28);
		Subject angular = new Subject("Angular", 28);

		// listSb = new ArrayList<>();
		listSb.add(java);
		listSb.add(angular);

		Result haRs1 = new Result(ha, java, 6);
		Result haRs2 = new Result(ha, angular, 7);

		Result vanRs1 = new Result(van, java, 9);
		Result vanRs2 = new Result(van, angular, 9);

		Result hieuRs1 = new Result(hieu, java, 9);
		Result hieuRs2 = new Result(hieu, angular, 10);

		Result anhRs1 = new Result(anh, java, 7);
		Result anhRs2 = new Result(anh, angular, 10);

		Result minhRs1 = new Result(minh, java, 7);
		Result minhRs2 = new Result(minh, angular, 9);

		// listRs = new ArrayList<>();

		listRs.add(haRs1);
		listRs.add(haRs2);

		listRs.add(vanRs1);
		listRs.add(vanRs2);

		listRs.add(hieuRs1);
		listRs.add(hieuRs2);

		listRs.add(minhRs1);
		listRs.add(minhRs2);

		listRs.add(anhRs1);
		listRs.add(anhRs2);

		System.out.println("Done");
	}

	public void fillRd(int size) {
		Course cl = new Course("Fresher 27");
		Course cl2 = new Course("Fresher 28");
		
		//listCl = new ArrayList<>();
		listC.add(cl);
		listC.add(cl2);

		Subject java = new Subject("Java", 28);
		Subject angular = new Subject("Angular", 28);
		
		//listSb = new ArrayList<>();
		listSb.add(java);
		listSb.add(angular);
		int i=1;
		for(i=1; i<=size; i++) {
			Random rd = new Random();
			
			String stName = "Quang Ha "+i;
			Student st = new Student(stName, "", cl2);
			listSt.add(st);
			
			int score = rd.nextInt(11);
			Result rs1 = new Result(st, java, score);
			listRs.add(rs1);
			
			Result rs2 = new Result(st, angular, score);
			listRs.add(rs2);
			
		}
		
		System.out.println("Created random "+(i-1)+" records");
	}

	public static void main(String[] args) {

		Data data = new Data();
		data.fillRd(200);
		
		System.out.println(data.getListC().size() + "/" + data.getListSt().size() + "/" + data.getListSb().size() + "/" + data.getListRs().size());
		for (Student st : data.getListSt())
			System.out.print(st);
		System.out.println("//////////////");
		for (Result rs : data.getListRs())
			System.out.print(rs);
	}

}
