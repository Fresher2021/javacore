Baitap_1: Tìm kiếm n sinh viên có điểm cao nhất

GroupChat_RMI_Socket: Chat nhóm, lưu message trên server
       - Sử dụng 2 luồng xử lý: RMI và Socket
       - Sử dụng 1 hàng đợi lưu tất cả message từ người dùng nhập
       - Sử dụng 1 danh sách lưu tất cả những message đã được gửi đi
       - RMI xử lý luồng nhập từ người dùng và lưu các message
       - Socket xử lý việc gửi dữ liệu đi cho những người dùng khác
    + Config RMI và Socket trong file ServerRun và ClientRun