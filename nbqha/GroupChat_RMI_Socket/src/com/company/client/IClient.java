package com.company.client;

import com.company.model.Message;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.UUID;

public interface IClient extends Remote {
    public UUID getUserIdRemote() throws RemoteException;
    public void retrieveMessage(Message msg) throws RemoteException;
}
