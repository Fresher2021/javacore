package com.company.server;

import com.company.client.IClient;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.UUID;

public class ServerRun {

    public static void main(String[] args) throws IOException {

        ServerImp server = new ServerImp();

        System.setProperty("java.rmi.server.port","1099");
        System.setProperty("java.rmi.server.hostname","192.168.1.241");
        Thread rmiThread = new Thread(new RMIServerThread("rmi://192.168.1.241/ABC", 1099, server));
        rmiThread.start();

        try (ServerSocket serverSocket = new ServerSocket(1100)) {
            System.out.println("Socket server is listening on port " + 1100);
            while (true) {
                Socket socket = serverSocket.accept();
                new SocketServerThread(socket, server).start();
                System.out.println("New client connected");
            }
        } catch (IOException ex) {
            System.out.println("Socket server exception: " + ex.getMessage());
            ex.printStackTrace();
        }
    }

}
