package com.company.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

public class Message implements Serializable {

    private UUID cId;
    private String cName;
    private String content;
    private LocalDateTime time;

    public Message(UUID cId, String cName, String content) {
        this.cId = cId;
        this.content = content;
        this.cName = cName;
        time = LocalDateTime.now();
    }

    public UUID getcId() {
        return cId;
    }

    public String getcName(){
        return this.cName;
    }

    public String getContent() {
        return content;
    }

    public String getTime() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        return time.format(formatter).toString();
    }

    @Override
    public String toString() {
        return "[" + cName + "]: " + content;
    }
}
