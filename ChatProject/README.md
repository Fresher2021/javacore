# Javacore project
----------------------
## Description
Build a chat application using RMI, Socket and Multithread

## How to run
1. Turn off firewall
2. Run Server.java file
3. Run Client.java file, enter user name with format : "User {number} " . In which, number is the number of client, start from 0 when server is started.

Ex: If you are the first person running Client.java, enter name: User 0, and then User 1, User 2,...

